describe('Pagina Inicial', () => {
  it('Visita a pagina inicial', () => {
    cy.visit('/')
    cy.contains('Jogo de Rede Social')
    cy.contains('Dashboard')
    cy.contains('Editar Humor')
    cy.contains('Pedido Introdução')
  })

    it('navega para Editar Humor', () => {
      cy.visit('/')
      cy.get('app-side-nav > mat-drawer-container > mat-drawer > div > mat-nav-list > a').eq(3).click();
      cy.url().should('includes', 'editarHumor')
  })


  it('navega para Pedido Introdução', () => {
    cy.visit('/')
    cy.get('app-side-nav > mat-drawer-container > mat-drawer > div > mat-nav-list > a').eq(5).click();
    cy.url().should('includes', 'pedirIntroducao')
})
})
