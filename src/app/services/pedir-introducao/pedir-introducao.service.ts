import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MessagesService } from '../messages/messages.service';

import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { PedirIntroducaoDto } from '../../dto/pedirIntroducao';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PedirIntroducaoService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json-patch+json'
    })
  };

  private url = environment.urlAddress;
  //private url = 'https://localhost:5001';
  private pedidoIntroUrl = '/api/Introducao/pedidoIntroducao'

  constructor(private messagesService: MessagesService, private http: HttpClient) { }


  postNovoPedidoIntroducao(pedidoIntro: PedirIntroducaoDto): Observable<void> {
    const url = this.url + this.pedidoIntroUrl;

    return this.http.post<PedirIntroducaoDto>(url, pedidoIntro, { observe: 'response' })
      .pipe(
        /* tap((response: HttpResponse<PedirIntroducaoDto>) => this.log(`resposta ${response.status}`)),*/
        catchError(this.handleError<any>('postNovoPedidoIntroducao'))
      );
  }

  private log(message: string) {
    this.messagesService.add(`Pedir Introducao Service: ${message}`);
  }

  //retrived from https://angular.io/tutorial/toh-pt6
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    }
  }
}
