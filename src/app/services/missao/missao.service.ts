import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MissaoDto } from 'src/app/dto/missao/missaoDto';
import { RegistarMissaoDto } from 'src/app/dto/missao/registarMissaoDto';
import { ApiService } from '../shared/api.service';

@Injectable({
  providedIn: 'root'
})
export class MissaoService {

  constructor(private api: ApiService) { }

  private uri: string = "api/Missao"


  registarMissao(body: RegistarMissaoDto): Observable<RegistarMissaoDto> {
    return this.api.create(`${this.uri}`, body) as Observable<RegistarMissaoDto>;
  }

  getMissoes(email: string, numeroPagina: number, totalPagina: number): Observable<HttpResponse<MissaoDto[]>> {
    return this.api.getDataWithHeaders(this.uri, { "email": email, "numeroPagina": numeroPagina, "tamanhoPagina": totalPagina }) as Observable<HttpResponse<MissaoDto[]>>;

  }
}
