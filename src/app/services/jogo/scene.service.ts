import { Humor } from './../../model/humor';
import { ElementRef, HostListener, Injectable, NgZone, OnDestroy } from '@angular/core';
import { JogadoJogoDto } from 'src/app/dto/jogador/jogadorJogoDto';
import {
  AmbientLight,
  BoxGeometry,
  CanvasTexture,
  ClampToEdgeWrapping,
  Color,
  CylinderGeometry,
  Group,
  Light,
  LinearFilter,
  Mesh,
  MeshBasicMaterial,
  MeshPhongMaterial,
  OrthographicCamera,
  PerspectiveCamera,
  PlaneGeometry,
  PointLight,
  Raycaster,
  Scene,
  SpotLight,
  Sprite,
  SpriteMaterial,
  TextureLoader,
  Vector2,
  Vector3,
  WebGLRenderer
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import Dados from "../../../assets/MockData/ligacoes.json";

@Injectable({ providedIn: 'root' })
export class SceneService implements OnDestroy {
  private canvas!: HTMLCanvasElement;
  private renderer!: WebGLRenderer;
  private camera!: PerspectiveCamera;
  private ortoCamera!: OrthographicCamera;
  private scene!: Scene;
  private light!: Light[];
  private orbitControls!: OrbitControls;
  private hoverContainer!: Group;

  public cubes: Group = new Group();
  private arrows: Group = new Group();
  private sprites: Group = new Group();

  player = new JogadoJogoDto();
  meuSet = new Set();
  meuMap = new Map();
  mouse = new Vector2();
  data_points = [];
  container;

  private frameId: number | null = null;

  color_array = [
    "#1f78b4",
    "#b2df8a",
    "#33a02c",
    "#fb9a99",
    "#e31a1c",
    "#fdbf6f",
    "#ff7f00",
    "#6a3d9a",
    "#cab2d6",
    "#ffff99"
  ]

  red_gradient = [
    "#FFEBEE",
    "#FFCDD2",
    "#E57373",
    "#E57373",
    "#EF5350",
    "#F44336",
    "#E53935",
    "#D32F2F",
    "#C62828",
    "#B71C1C"
  ]

  blue_gradient = [
    "#E3F2FD",
    "#BBDEFB",
    "#90CAF9",
    "#64B5F6",
    "#42A5F5",
    "#2196F3",
    "#1E88E5",
    "#1976D2",
    "#1565C0",
    "#0D47A1"
  ]

  public constructor(private ngZone: NgZone) {
  }

  public ngOnDestroy(): void {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
  }


  requestAnimationFrame(callback) {
    const requestAnimationFrame = window.requestAnimationFrame;
    requestAnimationFrame(callback);
  }

  public createScene(canvas: ElementRef<HTMLCanvasElement>): void {
    // The first step is to get the reference of the canvas element from our HTML document
    this.canvas = canvas.nativeElement;

    this.renderer = new WebGLRenderer({
      canvas: this.canvas,
      alpha: true,    // transparent background
      antialias: true // smooth edges
    });
    let width = window.innerWidth;
    let height = window.innerHeight;

    this.renderer.setSize(width, height);
    // this.renderer.shadowMap.enabled = true;
    this.container = document.getElementById("Threejs");
    this.container.appendChild(this.renderer.domElement);



    // create the scene
    this.scene = new Scene();
    this.scene.background = new Color(0x4C524F);

    this.hoverContainer = new Group();
    this.scene.add(this.hoverContainer);

    this.camera = new PerspectiveCamera(
      75, window.innerWidth / window.innerHeight, 1, 1000
    );
    this.camera.position.z = 15;
    this.camera.position.y = 10;
    this.camera.position.x = 1;



    //Foco de luz a acompanhar a camara
    var pointLight = new PointLight(0xffffff, 1, 100);
    pointLight.position.copy(this.camera.position);
    this.camera.add(pointLight);
    this.scene.add(this.camera);


    this.ortoCamera = new OrthographicCamera(
      width / -2, width / 2, height / 2, height / -2, -5000, 10000);
    this.ortoCamera.up = new Vector3(0, 0, -1);
    this.ortoCamera.lookAt(new Vector3(0, 0, -1));
    this.scene.add(this.ortoCamera);


    // soft white light
    this.createLight();

    document.addEventListener("mousemove", (event) => onMouseMove(event, this.camera, this.cubes, this.renderer, this.data_points, tooltip_state, $tooltip, $nome_tip, $email_tip, $group_tip));


    function onMouseMove(event, camera, cubes, renderer, points, toolTipState, toolTip, nomeTip, emailTip, groupTip) {
      event.preventDefault();
      //x
      let mouse3D;
      let rect = renderer.domElement.getBoundingClientRect();
      if (event.clientX && event.clientY) {
        const mouseX = (event.clientX - rect.left) / rect.width * 2.0 - 1.0;
        const mouseY = -(event.clientY - rect.top) / rect.height * 2.0 + 1.0;
        mouse3D = new Vector2(
          mouseX, mouseY
        );
      }
      const raycaster = new Raycaster();
      raycaster.setFromCamera(mouse3D, camera);

      const intersects = raycaster.intersectObjects(cubes.children, false);
      // console.log('SECT: ', intersects);
      if (intersects.length > 0) {
        // console.log('MAT: ', intersects[0].object['material'].color)
        //intersects[0].object.['material']color.setHex(Math.random() * 0xffffff);
        let pos = intersects[0].object.parent.children.indexOf(intersects[0].object);
        // console.log('PARENT', pos);
        // console.log('TOOL: ', points[pos])
        console.log('INTER: ', intersects[0]);
        highlightPoint(intersects[0], cubes);

        let mouse3D_ = new Vector2(
          event.clientX, event.clientY
        );

        // showTooltip(mouse3D_, points[pos], toolTipState, toolTip, nomeTip, emailTip, groupTip);
      } else {
        removeHighlights(cubes);
        //hideTooltip(toolTipState, toolTip, nomeTip, emailTip, groupTip);
      }

    }


    // Create controls
    this.createControls();

    // process data
    this.initRede();

    this.createFloor();

    this.scene.add(this.cubes);
    this.scene.add(this.arrows);
    this.scene.add(this.sprites);


  }

  private createFloor(): void {
    // Floor
    var floorGeometry = new PlaneGeometry(window.innerWidth, window.innerHeight, 20, 20);
    var floorMaterial = new MeshPhongMaterial({
      color: 0x07A259,
      specular: 0x000000,
      shininess: 100
    });

    var floor = new Mesh(floorGeometry, floorMaterial);
    floor.rotation.x = -0.5 * Math.PI;
    floor.receiveShadow = true;
    this.scene.add(floor);
  }

  private createControls(): void {
    const controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.orbitControls = controls;
  }

  private createLight(): void {
    const ambientLight = new AmbientLight(0x090909);
    ambientLight.intensity = 3;

    var spotLight = new SpotLight(0xAAAAAA);
    spotLight.position.set(15, 3, 8);
    spotLight.angle = Math.PI / 4;
    spotLight.castShadow = true;
    spotLight.shadow.bias = 0.0001;
    spotLight.shadow.mapSize.width = 2048; // Shadow Quality
    spotLight.shadow.mapSize.height = 2048; // Shadow Quality

    var spotLight2 = new SpotLight(0xAAAAAA);
    spotLight2.position.set(-10, 3, 8);
    spotLight2.angle = Math.PI / 4;
    spotLight2.castShadow = true;
    spotLight2.shadow.bias = 0.0001;
    spotLight2.shadow.mapSize.width = 2048; // Shadow Quality
    spotLight2.shadow.mapSize.height = 2048; // Shadow Quality

    this.light = [ambientLight, spotLight, spotLight2];
    this.scene.add(...this.light);
  }

  public animate(): void {
    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }

      window.addEventListener('resize', () => {
        this.resize();
      });
    });
  }

  public render(): void {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    this.renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
    this.renderer.clear();

    this.renderer.render(this.scene, this.camera);

    this.renderer.setViewport(10, window.innerHeight - 160 - 10, 240, 160);
    // this.renderer.render(this.scene, this.ortoCamera);
  }

  public resize(): void {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);

  }


  private initRede() {
    this.traverse(Dados, this.process, 0);

    // TODO -> instanciar variáveis para imprimir
    let radius = 4;
    let size = this.meuSet.size;
    let point_num = size; // numero de pontos
    let rad;
    let points = [];
    let x, y, z;
    let playersByLevel = new Array(size).fill(0);

    // TODO -> criar um array para saber quantos jogadores há em cada nível, p. ex [1,2,3,4] tem 1 jogador no nível 0, 2 no nível 1, 3 no nível 2...
    for (let player of this.meuMap) {
      playersByLevel[player[1]] += 1;
    }

    // TODO -> limpa níveis vazios
    for (let i = playersByLevel.length; i > 0; i--) {
      if (playersByLevel[i] == 0)
        playersByLevel.splice(i, 1);
    }

    // TODO -> Copia o array de jogadores por nível para o usar como contador (por cada jogador imprimido, reduz-se 1 ao número de jogadores desse nivel até chegar a 0)
    let contadorPorNivel = [...playersByLevel];


    // Random point in circle
    function randomPosition(player, radius) {
      // TODO -> Método para criar array com as posições x,y e z de cada jogador, para depois se passar para o tree.js
      if (player[1] === 0) {
        x = 0;
        z = 0;
        points.push([x, player[0].listLigacaoLength, z]);
      } else {
        rad = (Math.PI * 2) / (playersByLevel[player[1]] / contadorPorNivel[player[1]]);
        contadorPorNivel[player[1]] -= 1;

        x = Math.cos(rad) * (radius * player[1]) + 0;
        z = Math.sin(rad) * (radius * player[1]) + 0;

        points.push([x, player[0].listLigacaoLength, z]);
      }

      return [x, player[0].listLigacaoLength, z];
    }



    for (let player of this.meuMap) {
      let position = randomPosition(player, radius);
      let name = 'Nome: ' + player[0].nome;
      let email = 'Email: ' + player[0].email;
      let humor = 'Humor: ' + player[0].humor;
      let avatar = 'Avatar: ' + player[0].avatar;
      let group = player[1];
      let size = player[0].tags.length === 0 ? 0.5 : player[0].tags.length;
      let fromParent = player[0].fromParentLigacao;
      let toParent = player[0].toParentLigacao
      let point = { position, name, email, humor, avatar, size, group, fromParent, toParent };

      this.data_points.push(point);
    }

    //console.log('CUBES: ', this.cubes);



    let generated_points = this.data_points;

    let colors = [];
    for (let datum of generated_points) {
      // Set vector coordinates from data
      let vertex = new Vector3(datum.position[0], 0, datum.position[2]);
      let size = 1 + ((datum.size * 10) / 100);
      var geometry = new BoxGeometry(size, size, size);
      var pic = datum.avatar.split(':')[1].trim();

      if(pic === 'null'){
        pic = "https://thumbs.dreamstime.com/z/avatar-dj-outline-icon-blue-neon-style-signs-symbols-can-be-used-web-logo-mobile-app-ui-ux-black-background-155341109.jpg"
      }

      var material = new MeshBasicMaterial({ // Required For Shadows
        map: new TextureLoader().load(pic)
      });

      var cube = new Mesh(geometry, material);
      cube.position.x = datum.position[0];
      cube.position.z = datum.position[2];
      cube.position.y = datum.position[1] + size / 2;
      cube.castShadow = true;
      cube.receiveShadow = true;

      this.cubes.add(cube);

      // Adicionar Billboard com humor
      const texture = new CanvasTexture(this.canvas);
      // because our canvas is likely not a power of 2
      // in both dimensions set the filtering appropriately.
      texture.minFilter = LinearFilter;
      texture.wrapS = ClampToEdgeWrapping;
      texture.wrapT = ClampToEdgeWrapping;

      let map: any;
      
      switch (datum.humor.split(':')[1].trim()) {
        case 'FELIZ':
          map = new TextureLoader().load('../../../assets/images/sun.png');
          break;
        case 'TRISTE':
          map = new TextureLoader().load('../../../assets/images/rain.png');
          break;
        case 'ZANGADO':
          map = new TextureLoader().load('../../../assets/images/lightning.png');
          break;
        case 'NEUTRO':
          map = new TextureLoader().load('../../../assets/images/fog.png');
          break;
        default:
          map = new TextureLoader().load('../../../assets/images/sun.png');
          break;
      }

      const labelMaterial = new SpriteMaterial({
        map: map,
        color: 0xffffff
      });

      const label = new Sprite(labelMaterial);
      label.position.x = datum.position[0] + (size * 1.2);
      label.position.y = datum.position[1] + (size * 1.2);
      label.position.z = datum.position[2];

      // if units are meters then 0.01 here makes size
      // of the label into centimeters.
      const labelBaseScale = 0.001;
      label.scale.x = this.canvas.width * (labelBaseScale * 0.8);
      label.scale.y = this.canvas.height * labelBaseScale;

      this.sprites.add(label);

    }


    for (let i = 0; i < this.data_points.length; i++) {
      let edgeGeometry;
      let edgeMaterial;
      let edgeGeometry2;
      let edgeMaterial2;
      if (this.data_points[i].group !== 0) {
        //console.log('XXX ', data_points[0].position[1])
        let size = 1 + ((this.data_points[i].size * 10) / 100);
        if (this.data_points[i].group === 1) {

          //const length = radius;
          const length = (Math.sqrt(Math.pow((this.data_points[i].position[0] - this.data_points[0].position[0]), 2) +
            Math.pow(((this.data_points[i].position[1] + size / 2) - (this.data_points[0].position[1] + size / 2)), 2) +
            Math.pow((this.data_points[i].position[2] - this.data_points[0].position[2]), 2)));

          let forcaFromParent = this.data_points[i].fromParent;
          if (forcaFromParent === 100) {
            forcaFromParent = 99;
          }
          let forcaToParent = this.data_points[i].toParent;
          if (forcaToParent === 100) {
            forcaToParent = 99;
          }
          const hex = this.red_gradient[Math.trunc(forcaFromParent / 10)];
          const hex2 = this.blue_gradient[Math.trunc(forcaToParent / 10)];

          // Create a cylinder geometry
          edgeGeometry = new CylinderGeometry(0.01, 0.15, 0.7);

          // Create a red-colored material
          edgeMaterial = new MeshBasicMaterial({ color: hex });

          // Create a cylinder geometry
          edgeGeometry2 = new CylinderGeometry(0.15, 0.01, 0.7);

          // Create a blue-colored material
          edgeMaterial2 = new MeshBasicMaterial({ color: hex2 });

          const cylinder = new Mesh(edgeGeometry, edgeMaterial);
          const cylinder2 = new Mesh(edgeGeometry2, edgeMaterial2);


          // Set its position
          cylinder.position.set((0 + this.data_points[i].position[0]) / 2.0, ((this.data_points[i].position[1] + (size / 2)) + (this.data_points[0].position[1] + size / 2)) / 2.0, (0 + this.data_points[i].position[2]) / 2.0);
          cylinder2.position.set((0 + this.data_points[i].position[0]) / 2.0, ((this.data_points[i].position[1] + (size / 2)) + (this.data_points[0].position[1] + size / 2)) / 2.0, (0 + this.data_points[i].position[2]) / 2.0);

          // Set its orientation
          const angH = Math.atan2(this.data_points[i].position[0] - 0, this.data_points[i].position[2] - 0);
          const angV = Math.acos(((this.data_points[i].position[1] + (size / 2)) - (this.data_points[0].position[1] + size / 2)) / length);

          cylinder.rotateY(angH);
          cylinder.rotateX(angV);
          cylinder2.rotateY(angH);
          cylinder2.rotateX(angV);

          // Set its length
          cylinder.scale.set(1.0, length, 1.0);
          cylinder2.scale.set(1.0, length, 1.0);

          // Add it to the scene
          this.arrows.add(cylinder)
          this.arrows.add(cylinder2)
        } else {
          let playerPosition = 0;
          for (let player of this.meuMap) {
            let email = this.data_points[i].email.split(':')[1].trim();
            if (player[0].email == email) {
              break;
            }
            playerPosition++;
          }


          let count = 0;
          let parentPosition;
          for (let player of this.meuMap) {
            if (player[1] === this.data_points[i].group - 1) {
              //console.log('PAI: ', player);
              let parent = player;
            }
            if (count >= playerPosition - 1) {
              parentPosition = count;
              break;
            }
            count++;
          }


          const length2 = (Math.sqrt(Math.pow((this.data_points[playerPosition].position[0] - this.data_points[parentPosition].position[0]), 2) +
            Math.pow(((this.data_points[playerPosition].position[1] + size / 2) - (this.data_points[parentPosition].position[1] + size / 2)), 2) +
            Math.pow((this.data_points[playerPosition].position[2] - this.data_points[parentPosition].position[2]), 2)));


          let forcaFromParent = this.data_points[playerPosition].fromParent;
          if (forcaFromParent === 100) {
            forcaFromParent = 99;
          }
          let forcaToParent = this.data_points[playerPosition].toParent;
          if (forcaToParent === 100) {
            forcaToParent = 99;
          }
          const hex = this.red_gradient[Math.trunc(forcaFromParent / 10)];
          const hex2 = this.blue_gradient[Math.trunc(forcaToParent / 10)];

          let edgeGeometry3 = new CylinderGeometry(0.01, 0.15, 1);

          // Create a red-colored material
          let edgeMaterial3 = new MeshBasicMaterial({ color: hex2 });
          const cylinder3 = new Mesh(edgeGeometry3, edgeMaterial3);

          let edgeGeometry4 = new CylinderGeometry(0.15, 0.01, 1);

          // Create a red-colored material
          let edgeMaterial4 = new MeshBasicMaterial({ color: hex });
          const cylinder4 = new Mesh(edgeGeometry4, edgeMaterial4);

          // Set its position
          cylinder3.position.set((this.data_points[parentPosition].position[0] + this.data_points[playerPosition].position[0]) / 2.0, ((this.data_points[playerPosition].position[1] + (size / 2)) + (this.data_points[parentPosition].position[1] + (size / 2))) / 2.0, (this.data_points[playerPosition].position[2] + this.data_points[parentPosition].position[2]) / 2.0);
          cylinder4.position.set((this.data_points[parentPosition].position[0] + this.data_points[playerPosition].position[0]) / 2.0, ((this.data_points[playerPosition].position[1] + (size / 2)) + (this.data_points[parentPosition].position[1] + size / 2)) / 2.0, (this.data_points[playerPosition].position[2] + this.data_points[parentPosition].position[2]) / 2.0);

          // Set its orientation
          const angH2 = Math.atan2(this.data_points[parentPosition].position[0] - this.data_points[playerPosition].position[0], this.data_points[parentPosition].position[2] - this.data_points[playerPosition].position[2]);
          const angV2 = Math.acos(((this.data_points[parentPosition].position[1] + (size / 2)) - (this.data_points[playerPosition].position[1] + (size / 2))) / length2);
          cylinder3.rotateY(angH2);
          cylinder3.rotateX(angV2);
          cylinder4.rotateY(angH2);
          cylinder4.rotateX(angV2);

          // Set its length
          cylinder3.scale.set(1.0, length2, 1.0);
          cylinder4.scale.set(1.0, length2, 1.0);

          this.arrows.add(cylinder3)
          this.arrows.add(cylinder4)
        }

      }
    }
  }

  // Transforma JSON em MAP(Jogador, nível)
  private process(key, value, nivel) {

    // Cria instância de jogador
    switch (key) {
      case 'jogador':
        this.player.nome = value.nome;
        this.player.email = value.email;
        this.player.tags = value.tags;
        this.player.humor = value.humor;
        this.player.avatar = value.avatar;
        break;
      case 'fromParent':
        if (value === null) {
          this.player.fromParentLigacao = null;
          this.player.fromParentRelacao = null;
          this.player.fromParentTags = [];
        } else {
          this.player.fromParentLigacao = value.forcaLigacao;
          this.player.fromParentRelacao = value.forcaRelacao;
          this.player.fromParentTags = value.tags;
        }
        break;
      case 'toParent':
        if (value === null) {
          this.player.toParentLigacao = null;
          this.player.toParentRelacao = null;
          this.player.toParentTags = [];
        } else {
          this.player.toParentLigacao = value.forcaLigacao;
          this.player.toParentRelacao = value.forcaRelacao;
          this.player.toParentTags = value.tags;
        }
        break;
      case 'listLigacoes':
        this.player.listLigacaoLength = value.length;
        break;

      default:
        break;
    }

    // Quando chega à mudança de nível, guarda o this.player no map
    if (key === 'listLigacoes') {
      if (!this.meuSet.has(this.player.email)) {
        this.meuSet.add(this.player.email);
        this.meuMap.set(this.player, nivel);
      }

      // Cria novo this.player vazio
      this.player = new JogadoJogoDto();

    }

    //console.log('XXXXXXXXXXXXXXXXXXXXXX');
    // TODO -> no meuMap tens {jogador, nível}
    // console.log('MAP: ', this.meuMap);
    //console.log('XXXXXXXXXXXXXXXXXXXXXX');
  }



  // Método recursivo para correr JSON recebido da API
  private traverse(o, func, nivel) {
    for (var i in o) {
      // Chama função process para "trabalhar os dados recebidos"
      func.apply(this, [i, o[i], nivel]);
      if (o[i] !== null && typeof (o[i]) == "object") {
        // Descer um nível
        if (i === 'listLigacoes') {
          nivel++;
        }
        this.traverse(o[i], func, nivel);
      }
    }
    nivel--;
  }

}


function highlightPoint(mat, cubes) {
  removeHighlights(cubes);
  mat.object['material'].color.setHex(0xF40A38);

}
function removeHighlights(cubes: Group) {
  cubes.children.forEach(cube => cube['material'].color.setHex(0xFCFCFC));
}

let circle_sprite = new TextureLoader().load("./images/download.png");

let tooltip_state = { display: "none" }

let tooltip_template = document.createRange().createContextualFragment(`<div id="tooltip" style="display: none; position: absolute; pointer-events: none; font-size: 13px; width: 120px; text-align: center; line-height: 1; padding: 6px; background: white; font-family: sans-serif; z-index:999;">
      <div id="nome_tip" style="padding: 4px; margin-bottom: 4px;"></div>
      <div id="email_tip" style="padding: 4px;"></div>
      <div id="group_tip" style="padding: 4px;"></div>
    </div>`);
document.body.append(tooltip_template);

let $tooltip = document.querySelector('#tooltip');
let $nome_tip = document.querySelector('#nome_tip');
let $email_tip = document.querySelector('#email_tip');
let $group_tip = document.querySelector('#group_tip');


function showTooltip(mouse_position: any, datum: any, tooltip_state, toolTip, nomeTip, emailTip, groupTip) {
  let tooltip_width = 140;
  let x_offset = -tooltip_width / 2;
  let y_offset = 30;
  tooltip_state.display = "block";
  tooltip_state.left = mouse_position.x + x_offset;
  tooltip_state.top = mouse_position.y + y_offset;
  tooltip_state.name = datum.name;
  tooltip_state.email = datum.email;
  tooltip_state.group = datum.group;
  updateTooltip(toolTip, tooltip_state, nomeTip, emailTip, groupTip);

}

function updateTooltip(toolTip, tooltip_state, nome_tip, email_tip, group_tip) {
  console.log("x: " + tooltip_state.left + " - y: " + tooltip_state.top);
  console.log('ZZZZZZ: ', tooltip_state);
  console.log('TIP: ', toolTip.style);
  toolTip.style.display = tooltip_state.display;
  console.log('YYYYYYYYYYY: ', tooltip_state.display);
  toolTip.style.left = tooltip_state.left + 'px';
  toolTip.style.top = tooltip_state.top + 'px';
  nome_tip.innerText = tooltip_state.name;
  nome_tip.style.background = 0xEBFCF7;
  email_tip.innerText = tooltip_state.email;
  group_tip.innerText = `Group ${tooltip_state.group}`;
}

function hideTooltip(toolTip, tooltip_state, nome_tip, email_tip, group_tip) {
  tooltip_state.display = "none";
  updateTooltip(toolTip, tooltip_state, nome_tip, email_tip, group_tip);
}

