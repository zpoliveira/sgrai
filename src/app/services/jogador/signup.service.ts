import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RegistarJogadorDto } from 'src/app/dto/registarJogadorDto';
import { Jogador } from "../../model/jogador";
import { Observable, of } from "rxjs";
import * as url from "url";
import { environment } from "../../../environments/environment";
import { ApiService } from "../shared/api.service";

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json-patch+json',
      'Access-Control-Allow-Origin': '*'/*'application/json'*/
    })
  };

  private BaseURL = "api/jogador";

  constructor(private http: HttpClient, private api: ApiService) { }

  registarJogador(jogador: RegistarJogadorDto): Observable<RegistarJogadorDto> {
    return this.api.create(this.BaseURL, jogador) as Observable<RegistarJogadorDto>;

  }

  comparePasswords(fb: FormGroup) {
    let confirmPswrdCtrl = fb.get('ConfirmPassword');
    //passwordMismatch
    //confirmPswrdCtrl.errors={passwordMismatch:true}
    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if (fb.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }

  //retrived from https://angular.io/tutorial/toh-pt6
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    }
  }
}
