import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EditarHumorService } from './editar-humor.service';
import { Humor } from '../../model/humor'
import { AtualizarHumorDto } from '../../dto/atualizarHumorDto';
import { EditarHumorDto } from '../../dto/editarHumorDto';

describe('EditarHumorService', () => {
  let service: EditarHumorService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EditarHumorService]
    });
    service = TestBed.inject(EditarHumorService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('deve obter lista Estados de Humor', async () => {
    const mockHumor: Humor[] = [
      { estadoHumor: 'TRISTE' },
      { estadoHumor: 'NEUTRO' },
      { estadoHumor: 'FELIZ' }];
    (await service.getEstadosHumor()).subscribe(lista => {
      expect(lista.length).toBe(3);
      expect(lista).toEqual(mockHumor);
    });

    const request = httpMock.expectOne(`https://localhost:5001/api/Jogador/estadosHumor`);
    expect(request.request.method).toBe('GET');
    request.flush(mockHumor);

  });


  it('deve atualizar estado humor', async () => {
    const novoHumor: EditarHumorDto = { email: 'teste@email.com', humor: 'NEUTRO' };
    const expectedResponse: AtualizarHumorDto = { nome: 'User Teste', humor: 'NEUTRO' };
    (await service.patchEstadoHumor(novoHumor)).subscribe(resposta => {
      expect(resposta).toEqual(expectedResponse);
    });

    const request = httpMock.expectOne(`https://localhost:5001/api/Jogador/humor`);
    expect(request.request.method).toBe('PATCH');
    request.flush(expectedResponse);

  });

  afterEach(() => {
    httpMock.verify();
  });

});
