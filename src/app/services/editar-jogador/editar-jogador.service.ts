import { Jogador } from './../../model/jogador';
import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EditarJogadorDto } from 'src/app/dto/editarJogadorDto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class EditarJogadorService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json-patch+json',
      'Access-Control-Allow-Origin': '*'
    })
  };



  // private url = ' https://localhost:5001/api/Jogador';
  private url = environment.urlAddress;



  constructor(private http: HttpClient) { }

  getJogadorByEmailAsync(email: string) {
    const url = this.url + `/api/Jogador/pesquisarJogadores?email=${email}`;
    console.log('URL: ', url);
    return this.http.get<Jogador[]>(url, this.httpOptions);

  }

  updateUtilizador(jogador: EditarJogadorDto) {
    const url = `${this.url}/api/Jogador/${jogador.email}`;
    return this.http.put(url, jogador);

  }

}
