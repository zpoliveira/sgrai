import { Jogador } from '../../model/jogador';
import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EditarJogadorDto } from 'src/app/dto/editarJogadorDto';
import { PedidoLigacaoDto } from 'src/app/dto/pedidoLigacaoDto';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class PesquisarJogadorService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json-patch+json',
      'Access-Control-Allow-Origin': '*'
    })
  };



  // private url = ' https://localhost:5001/api';
  private url = environment.urlAddress;




  constructor(private http: HttpClient) { }

  pesquisarJogadores(parametros: string[]) {
    let array = [];
    for (var [key, value] of Object.entries(parametros)) {
      if (value !== '') {
        array.push({ key, value });
      }
    }

    let url = `${this.url}/api/Jogador/pesquisarJogadores?`;

    for (let i = 0; i < array.length; i++) {
      url += `${array[i].key}=${array[i].value}&`;
    }

    const editedURL = url.slice(0, -1)


    return this.http.get<Jogador[]>(editedURL, this.httpOptions);

  }

  pedidoLigacao(pedido: PedidoLigacaoDto) {
    const url = `${this.url}/api/Introducao/pedidoLigacao`;
    return this.http.post(url, pedido, this.httpOptions);

  }

}
