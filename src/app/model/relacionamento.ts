export interface Relacionamento {
    emailDe:string;
    emailPara: string;
    nome: string;
    forcaLigacao: number;
    forcaRelacao: number;
    avatar: string;
    tags:string[];
}