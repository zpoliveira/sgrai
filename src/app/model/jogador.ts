export interface Jogador {
   
     nome: string;
     email: string;
     password: string;
     telefone: string;
     urlFacebook: string;
     urlLinkedIn: string;
     dataNascimento: string;
     humor: string;
     nacionalidade: string;
     cidadeResidencia: string;
     descricaoBreve: string;
     avatar: File;
     tagList: string[];
  }