import { Component, EventEmitter, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { EditarRelacionamentoDto } from 'src/app/dto/Relacionamento/editarRelacionamentoDto';


@Component({
  selector: 'app-editar-relacionamento',
  templateUrl: './editar-relacionamento.component.html',
  styleUrls: ['./editar-relacionamento.component.scss']
})
export class EditarRelacionamentoComponent implements OnInit {

  @Output() submitClicked = new EventEmitter<any>();

  relacionamento: EditarRelacionamentoDto;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  form: FormGroup;

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<EditarRelacionamentoComponent>,
    @Inject(MAT_DIALOG_DATA) relacionamento: EditarRelacionamentoDto) {

    this.relacionamento = relacionamento
  }


  @ViewChild('chipListRelacionamenroEditar', { static: true }) chipListRelacionamenroEditar;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];

  ngOnInit(): void {

  }

  save() {
    this.submitClicked.emit(this.relacionamento);
    //this.dialogRef.close(this.relacionamento);
  }

  close() {
    this.dialogRef.close();
  }

  addTag(event: MatChipInputEvent) {
    const input = event.input;
    const value = event.value;
    // Add language
    if ((value || '').trim() && this.relacionamento.LstTags.indexOf(value.trim()) < 0) {
      this.relacionamento.LstTags.push(value.trim())
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }

  }

  removeTag(tag: string): void {
    const index = this.relacionamento.LstTags.indexOf(tag);
    if (index >= 0) {
      this.relacionamento.LstTags.splice(index, 1);
    }
  }


}
