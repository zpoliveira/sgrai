import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, FormControl, FormGroupDirective, Validators, NgForm } from '@angular/forms';
import { ErrorMatcher } from '../../error-matcher';
import { PedirIntroducaoService } from '../../services/pedir-introducao/pedir-introducao.service';
import { PedirIntroducaoDto } from '../../dto/pedirIntroducao';
import { MessagesService } from '../../services/messages/messages.service';

@Component({
  selector: 'app-pedir-introducao',
  templateUrl: './pedir-introducao.component.html',
  styleUrls: ['./pedir-introducao.component.scss']
})
export class PedirIntroducaoComponent implements OnInit {

  dadosInt!: FormGroup;

  matcher = new ErrorMatcher();
  formBuilder = new FormBuilder();

  constructor(private introService: PedirIntroducaoService, private messagesService: MessagesService) {

  }

  ngOnInit() {
    this.dadosInt = this.formBuilder.group({
      emailJogadorRequisitante: new FormControl('', [Validators.required, Validators.email]),
      emailJogadorIntermediario: new FormControl('', [Validators.required, Validators.email]),
      emailJogadorObjectivo: new FormControl('', [Validators.required, Validators.email]),
      forcaLigacao: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(10)])
    });
  }

  async onFormSubmit(formDirective: FormGroupDirective) {
    if (this.dadosInt.valid) {

      let pedIntro = (this.dadosInt.value as PedirIntroducaoDto);
      console.log(pedIntro);

      (this.introService.postNovoPedidoIntroducao(pedIntro))
        .subscribe();

      formDirective.resetForm();
      this.dadosInt.reset();

    } else {
      //console.log("Erro");
      throw Error("Error");
    }


  }

}
