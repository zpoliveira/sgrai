import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { JogadorSugestaoDto } from 'src/app/dto/jogador/jogadorSugestaoDto';
import { RegistarMissaoDto } from 'src/app/dto/missao/registarMissaoDto';
import { PaginacaoDto } from 'src/app/dto/paginacaoDto';
import { JogadorService } from 'src/app/services/jogador/jogador.service';
import { MissaoService } from 'src/app/services/missao/missao.service';

@Component({
  selector: 'app-lista-jogadores-sugestao',
  templateUrl: './lista-jogadores-sugestao.component.html',
  styleUrls: ['./lista-jogadores-sugestao.component.scss']
})
export class ListaJogadoresSugestaoComponent implements OnInit {

  private pageSize = 10;
  private currentPage = 0;
  private totalSize = 0;
  emailDe: string = '1080510@isep.ipp.pt';
  sugestoes: JogadorSugestaoDto[] | any[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;



  constructor(private jogadorService: JogadorService, private missaoService: MissaoService) { }

  ngOnInit(): void {


  }

  ngAfterViewInit() {
    this.paginator.showFirstLastButtons = true;
    this.paginator.pageSize = this.pageSize;
    this.paginator.length = this.totalSize;
    this.paginator.pageSizeOptions = [10, 50, 100];
    this.paginator.pageIndex = this.currentPage;
    this.paginator.page.subscribe(x => this.handlePage(x));
    //this.getRelacionamentos();
  }

  private getSugestoes() {
    this.jogadorService.getSugestoes(this.emailDe, this.currentPage + 1, this.pageSize)
      .subscribe(x => {
        //console.log(x.headers);
        let jsonObj: any = JSON.parse(x.headers.get("x-paginacao"));
        let paginacao: PaginacaoDto = <PaginacaoDto>jsonObj;
        this.sugestoes = x.body;
        this.currentPage = paginacao.numeroPagina - 1;
        this.pageSize = paginacao.tamanhoPagina;
        this.totalSize = paginacao.contagemTotal;
        this.paginator.length = paginacao.contagemTotal;
        //return this.relacionamentos;
      });
  }

  lerSugestoes() {
    console.log(this.emailDe);
    this.getSugestoes();
  }

  handlePage(e: PageEvent) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.getSugestoes();
  }


  escolherJogadorObjectivo(sugestao: JogadorSugestaoDto) {
    console.log(this.emailDe);
    let body: RegistarMissaoDto = {
      GrauDificuldade: 1,
      JogadorDesafiado: this.emailDe,
      JogadorObjectivo: sugestao.email,
    }
    this.missaoService.registarMissao(body)
      .subscribe(x => {
        this.sugestoes.forEach((item, index) => {
          if (item.email === sugestao.email) this.sugestoes.splice(index, 1);
        });
      }
      )
  }
}
