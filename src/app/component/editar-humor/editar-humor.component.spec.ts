import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, By } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EditarHumorComponent } from './editar-humor.component';
import { EditarHumorService } from '../../services/editar-humor/editar-humor.service';
import { MessagesService } from '../../services/messages/messages.service';





describe('EditarHumorComponent', () => {
  let component: EditarHumorComponent;
  let fixture: ComponentFixture<EditarHumorComponent>;

  class EditarHumorSrv { };
  class MessagesSrv { };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        EditarHumorComponent
      ],
      imports: [
        ReactiveFormsModule,
        BrowserModule,
        FormsModule
      ],
      providers: [
        { provide: EditarHumorService, useClass: EditarHumorSrv },
        { provide: MessagesService, useClass: MessagesSrv }

      ]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(EditarHumorComponent);
      component = fixture.componentInstance;

    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
