import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMissoesComponent } from './lista-missoes.component';

describe('ListaMissoesComponent', () => {
  let component: ListaMissoesComponent;
  let fixture: ComponentFixture<ListaMissoesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaMissoesComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMissoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
