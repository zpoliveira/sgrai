import { Jogador } from '../../model/jogador';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common'
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { PesquisarJogadorService } from '../../services/pesquisar-jogador/pesquisar-jogador.service';
import { PedidoLigacaoDto } from '../../dto/pedidoLigacaoDto';


@Component({
  selector: 'app-pesquisar-jogador',
  templateUrl: './pesquisar-jogador.component.html',
  styleUrls: ['./pesquisar-jogador.component.scss']
})
export class PesquisarJogadorComponent implements OnInit {

  pesquisarJogadorForm: FormGroup;
  addForcaLigacaoForm: FormGroup;
  jogadores: Jogador[] = [];
  displayedColumns = ['nome', 'email', 'humor', 'facebookURL', 'linkedInURL', 'telefone', 'dataNascimento', 'nacionalidade', 'cidadeResidencia'];
  dataSource: MatTableDataSource<Jogador> = new MatTableDataSource(this.jogadores);
  clickedRow: { nome: string, email: string };


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private location: Location, private jogadorService: PesquisarJogadorService) { }

  ngOnInit(): void {
    this.initForm();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  initForm() {
    this.pesquisarJogadorForm = new FormGroup({
      nome: new FormControl('', Validators.maxLength(40)),
      email: new FormControl('', Validators.email),
      humor: new FormControl(''),
      urlFacebook: new FormControl('', Validators.pattern(new RegExp("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$"))),
      urlLinkedIn: new FormControl('', Validators.pattern(new RegExp("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$"))),
      telefone: new FormControl('', [Validators.minLength(1), Validators.maxLength(14)]),
      dataNascimento: new FormControl('', Validators.pattern(new RegExp("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$"))),
      nacionalidade: new FormControl('', [Validators.minLength(1), Validators.maxLength(40)]),
      cidadeResidencia: new FormControl('', [Validators.minLength(1), Validators.maxLength(40)])
    });
    this.addForcaLigacaoForm = new FormGroup({
      forca: new FormControl('', [Validators.max(100), Validators.min(1)])
    })
  }

  back(): void {
    this.location.back()
  }


  procurar(): void {
    this.jogadorService.pesquisarJogadores(this.pesquisarJogadorForm.value)
      .subscribe(jogadores => {
        this.jogadores = jogadores;
        return this.jogadores;
      });
  }

  pedido: PedidoLigacaoDto;
  fazerLigacao(): void {
    this.pedido = { 'emailJogadorRequisitante': '1161335@isep.ipp.pt', 'emailJogadorObjectivo': this.clickedRow.email, 'forcaLigacao': this.addForcaLigacaoForm.value['forca'] };
    this.jogadorService.pedidoLigacao(this.pedido).subscribe(e => e);
  }

  onRowClicked(row) {
    console.log('Row clicked: ', row);
  }

}
