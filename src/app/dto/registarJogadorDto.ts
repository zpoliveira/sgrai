export interface RegistarJogadorDto {
  nome: string;
  email: string;
  password: string;
  telefone: number;
  urlFacebook: string;
  urlLinkedIn: string;
  dataNascimento: number;
  humor: string;
  nacionalidade: string;
  cidadeResidencia: string;
  descricaoBreve: string;
  avatar: File;
  taglist: string[];
}
