export class PaginacaoDto { 
    contagemTotal: number;
    tamanhoPagina: number;
    numeroPagina: number;
    paginasTotal:number;
    temProxima:boolean;
    temAnterior:boolean;

    constructor() {
        this.numeroPagina=1;
        this.tamanhoPagina = 10;
      } 
}  