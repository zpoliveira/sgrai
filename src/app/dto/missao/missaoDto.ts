export interface MissaoDto {
    jogadorDesafiadoEmail: string;
    jogadorObjectivoEmail: string;
    jogadorObjectivoNome: string;
    jogadorObjectivoAvatar: string;
    grauDificuldade:number;
}