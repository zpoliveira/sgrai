export interface RegistarMissaoDto{
    JogadorDesafiado:string;
    JogadorObjectivo:string;
    GrauDificuldade:number;
}