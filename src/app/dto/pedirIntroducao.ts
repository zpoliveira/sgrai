export interface PedirIntroducaoDto { 
    emailJogadorRequisitante: string;
    emailJogadorIntermediario: string;
    emailJogadorObjectivo: string;
    forcaLigacao: number;
}  
