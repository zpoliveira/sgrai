export interface EditarRelacionamentoDto {
  Nome: string
  EmailJogadorDe: string;
  EmailJogadorPara: string;
  Forca: number;
  LstTags: string[];
}
