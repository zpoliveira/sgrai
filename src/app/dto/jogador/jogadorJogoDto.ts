export class JogadoJogoDto{
  nome:string ="";
  email:string ="";
  humor: string = "";
  avatar: string = "";
  tags: string[] = [];
  fromParentLigacao:number = 0;
  fromParentRelacao:number = 0;
  fromParentTags:string[] = [];
  toParentLigacao:number = 0;
  toParentRelacao:number = 0;
  toParentTags:string[] = [];
  listLigacaoLength: number = 0;
}