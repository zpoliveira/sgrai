export interface JogadorSugestaoDto{
    avatar:string;
    email:string;
    nome:string;
    tags:string[];
}